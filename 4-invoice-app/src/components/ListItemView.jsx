import PropTypes from 'prop-types';
import RowItem from "./RowItem"

const ListItemView = ({title,items,handlerDeteleItem}) => {
    return (
        <>
            <h3>{title}</h3>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    {items.map(({ id, product, price, quantity }) => (
                        <RowItem
                        key={id}
                        id = {id}
                        product={product} 
                        price={price} 
                        quantity ={quantity}
                        handlerDeteleItem={ handlerDeteleItem }
                        />
                    ))}

                </tbody>
            </table>
        </>
    )
}
ListItemView.propTypes = {
    title:PropTypes.string.isRequired,
    items:PropTypes.array.isRequired
}

export default ListItemView

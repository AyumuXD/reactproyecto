import PropTypes from 'prop-types'; 
import { Title } from './Title';
import { UserDetails } from './UserDetails';
export const HelloWorld =  ({user,id, title}) => {

    console.log(title)

    return (
        <>
            <Title title={title}/>
            <UserDetails user={user}
            id={id}/>
        </>
    )
}

HelloWorld.propTypes = {
    title:PropTypes.string.isRequired,
    id:PropTypes.number,
    user:PropTypes.any.isRequired

}

HelloWorld.defaultProps = {
    title: "Hola mundo",
    id: 1,
    user: "undefined"
}
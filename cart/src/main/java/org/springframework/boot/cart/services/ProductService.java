package org.springframework.boot.cart.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.cart.models.Product;
import org.springframework.boot.cart.repositories.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductService implements IProductService{

    @Autowired
    private ProductRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll(){
        return this.repository.findAll();
    }
}

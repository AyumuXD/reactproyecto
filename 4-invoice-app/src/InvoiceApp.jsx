import { getInvoce, calculateTotal } from "./services/getInvoce"
import ClientView from "./components/ClientView";
import CompanyView from "./components/CompanyView";
import InvoceView from "./components/InvoceView";
import ListItemView from "./components/ListItemView";
import TotalView from "./components/TotalView";
import { useEffect, useState } from "react";
import FormItemView from "./components/FormItemView";

const invoiceInitial = {
    id: 0,
    name: '',
    client: {
        name: '',
        lastName: '',
        address: {
            country: '',
            city: '',
            street: '',
            number: 0
        }
    },
    company: {
        name: '',
        fiscalNumber: 0,
    },
    items: []
}

export const InvoiceApp = () => {

    const [activeForm, setActiveForm] = useState(false);

    const [total,setTotal] =useState(0);

    const [counter, setCounter] = useState(4);

    const [invoice,setInvoice] = useState(invoiceInitial);

    const [items, setItems] = useState([]);

    

    const { id, name, client, company, items: itemsInitial } = invoice;

    

    useEffect(() =>{
        const data = getInvoce();
        console.log(data);
        setInvoice(data);
        setItems(data.items)
    },[]);

    

    useEffect(() =>{
        // console.log("counter actualizado")
    },[counter]);

    useEffect(() =>{
        // console.log("counter actualizado")
    },[counter]);

    useEffect(() =>{
        // console.log("counter actualizado")
        setTotal(calculateTotal(items))
    },[items]);
    

    

    const handlerAddInvoiceItems = ( {product,price,quantity} ) => {

        setItems([...items, {
            id: counter,
            product: product.trim(),
            price: +price.trim(),
            quantity: +quantity.trim()
        }]);
        setCounter(counter + 1)
    }

    const handlerDeteleItem = (id) =>{
        setItems(items.filter(item => item.id !== id))
    }

    const onActiveForm = () =>{
        setActiveForm(!activeForm)
    }

    return (
        <>

            <div className="container my-2">
                <div className="card">
                    <div className="card-header">
                        Ejemplo Factura
                    </div>
                    <div className="card-body">

                        <InvoceView
                            id={id} name={name}
                        />

                        <div className="row my-3">
                            <div className="col">

                                <ClientView
                                    title="Datos del cliente"
                                    client={client}

                                />

                            </div>
                            <div className="col">

                                <CompanyView
                                    title="Datos de la empresa"
                                    company={company} />

                            </div>
                        </div>

                        <ListItemView
                            title="Producto de la factura"
                            items={items}
                            handlerDeteleItem= {handlerDeteleItem}
                        />
                        <TotalView total={total} />
                        

                        <button 
                        onClick={ onActiveForm } 
                        className="btn btn-secondary">{!activeForm ? 'Agregar Item' : 'Ocultar formulario'}</button>
                        {!activeForm  || <FormItemView handler={ handlerAddInvoiceItems }  />}
                        

                    </div>
                </div>
            </div>
        </>
    )
}

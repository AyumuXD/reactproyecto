import PropTypes from 'prop-types';
const CompanyView = ({company,title}) => {
    const { name: nameCompany, fiscalNumber } = company;
    return (
        <>
            <h3>{title}</h3>
            <ul className="list-group">
                <li className="list-group-item active">{nameCompany}</li>
                <li className="list-group-item">{fiscalNumber}</li>
            </ul>
        </>
    )
}

CompanyView.propTypes ={
    company: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired
}

export default CompanyView

import { useEffect, useState } from "react";
import Swal from 'sweetalert2'

const FormItemView = ({handler}) => {
    const [formItemsState, setFormItemsState] = useState({
        product:'',
        price:'',
        quantity:'',
    });

    

    const { product, price, quantity} = formItemsState;

    useEffect(() =>{
        // console.log("Precio actualizado")
    },[price]);

    useEffect(() =>{
        // console.log("el formItemsState actualizado")
    },[formItemsState]);

    const onInputChange = ({ target:{name,value} }) => {
        setFormItemsState({
            ...formItemsState,
            [name]:value
        })
    }

    const onInvoiceSubmit = (event) => {
        event.preventDefault();

        if (product.trim().length <= 1) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No puede estar vacio el producto',
            });
            return;
        }

        if (isNaN(price.trim())) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'La cantidad acepta solamente numero',
            });
            return;
        }
        if (price.trim().length < 1) {

            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No puede estar vacio el precio',
            });
            return
        }
        if (isNaN(quantity.trim())) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'La cantidad acepta solamente numero',
            });
            return;
        }
        if (quantity.trim().length < 1) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No puede estar vacio la cantidad',
            });
            return;
        }
        handler(formItemsState);


        
        setFormItemsState({
            product:'',
            price:'',
            quantity:'',
        })
        
    }

    return (
        <>
            <form className="w-50" onSubmit={onInvoiceSubmit}>
                <input type="text" name="product" placeholder="Productos" value={product} className="form-control m-3"
                    onChange={onInputChange} />
                <input type="text" name="price" placeholder="Precio" value={price} className="form-control m-3"
                    onChange={onInputChange} />
                <input type="text" name="quantity" placeholder="Cantidad" value={quantity} className="form-control m-3"
                    onChange={onInputChange} />
                <button type="submit" className="btn btn-primary m-3">Guardar</button>
            </form>
        </>
    )
}

export default FormItemView

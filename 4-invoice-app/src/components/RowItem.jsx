import PropTypes from 'prop-types';
const RowItem = ({ id, product, price, quantity,handlerDeteleItem}) => {
    return (
        <>
            <tr>
                <td>{product}</td>
                <td>{price}</td>
                <td>{quantity}</td>
                <td> <button onClick={()=>  handlerDeteleItem(id)} className='btn btn-danger'>Eliminar</button> </td>
            </tr>
        </>
    )
}

RowItem.propTypes = {
    product: PropTypes.string.isRequired,
    price:PropTypes.number.isRequired,
    quantity:PropTypes.number.isRequired
}

export default RowItem

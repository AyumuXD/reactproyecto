import { useState } from "react";

export const Counter = ({value}) => {

  const [valor, setValor] = useState(value);
  const counterIncrement = () => setValor(c => c + 1);
  return (
    <>
      <h2>El valor del contador es {valor}</h2>
      <button 
      onClick={counterIncrement }>Incrementar contador +1</button>
    </>
  )
}


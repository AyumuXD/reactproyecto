package org.springframework.boot.cart.repositories;

import org.springframework.boot.cart.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {
}

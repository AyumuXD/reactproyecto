package com.mx.backenduser.services;

import com.mx.backenduser.models.User;
import com.mx.backenduser.respotories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<User>findAll(){
        return this.repository.findAll();
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<User> findById(Long id){
        return this.repository.findById(id);
    }

    @Override
    @Transactional
    public User saveUser(User user){
        return this.repository.save(user);
    }
    @Override
    @Transactional
    public Optional<User> update(User user, Long id){
        Optional<User> o = this.findById(id);
        User userOptional = null;
        if(o.isPresent()){
            User userDb = o.orElseThrow();
            userDb.setUsername(user.getUsername());
            userDb.setEmail(user.getEmail());
            userOptional = this.saveUser(userDb);
        }

        return Optional.ofNullable(userOptional);
    }

    @Override
    @Transactional
    public void remove(Long id){
        this.repository.deleteById(id);
    }


}

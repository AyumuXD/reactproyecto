import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { HelloWorld } from './components/HelloWorld'


//const h1 = React.createElement('div',null,React.createElement('ul', null,React.createElement('li',null,"item1")));
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <HelloWorld 
    user = {'Pepe'} 
    id={1 }
    title= 'Hola mundo'/>
  </React.StrictMode>
)

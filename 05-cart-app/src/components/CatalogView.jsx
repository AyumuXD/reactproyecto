import { useEffect, useState } from "react";
import { getProducts } from "../services/productService";
import CatalogItemView from "./CatalogItemView";
import axios from "axios";

const baseURL = "http://localhost:8080/api/products";
const CatalogView = ({ handler }) => {
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState("");


    const productAxio = () => {
        getProducts().then(res => {
            setProducts(res)


        })
    }

    useEffect(
        () => {
            productAxio();
        }, []
    )
    return (
        <>

            {products?.length === 0 ? <div className="alert alert-warning" role="alert">
                No hay datos que mostrar
            </div>
                : <>

                    <div className="row">
                        {products.map(prod => (
                            <CatalogItemView
                                handler={handler}
                                key={prod.id}
                                id={prod.id}
                                name={prod.name}
                                description={prod.description}
                                price={prod.price}
                            />
                        ))}
                    </div>
                </>
            }

        </>
    )
}

export default CatalogView

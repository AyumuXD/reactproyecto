package org.springframework.boot.cart.services;

import org.springframework.boot.cart.models.Product;

import java.util.List;

public interface IProductService {
    List<Product> findAll();
}

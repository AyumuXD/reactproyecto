package com.mx.backenduser.respotories;

import com.mx.backenduser.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}

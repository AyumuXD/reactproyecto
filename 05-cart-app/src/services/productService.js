import axios from "axios";

export const getProducts = async () => {
    const baseURL = "http://localhost:8080/api/products";
    try {
        return await (await axios.get(baseURL)).data
    } catch (e) {
        return e;
    }

}
export const calculateTotal = (items) => {
    return items.reduce((accumulator, item) => accumulator + item.product.price * item.quantity, 0)
}
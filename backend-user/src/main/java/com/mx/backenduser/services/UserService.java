package com.mx.backenduser.services;

import com.mx.backenduser.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll();
    Optional<User> findById(Long id);
    User saveUser(User user);

    Optional<User> update(User user, Long id);

    void remove(Long id);
}

import { useEffect, useState } from "react"
import { calculateTotal } from "../services/productService";
import { useNavigate } from "react-router-dom";

const CartView = ({ items,handlerDelete }) => {

    const navigate = useNavigate();

    const [total, setTotal] = useState(0);

    useEffect( () => {
        setTotal(calculateTotal(items));
        sessionStorage.setItem('cart',JSON.stringify(items))
    },[items])

    const onDeleteProduct = (id) => {
        handlerDelete(id)
    }
    const onCatalog = () => {
        navigate('/catalog')
    }
    return (
        <>
            <h3>Carro de compra</h3>
            <table className="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>

                <tbody>
                    {items.map(item => (
                        <tr key={item.product.id}>
                            <td>{item.product.name}</td>
                            <td>{item.product.price}</td>
                            <td>{item.quantity}</td>
                            <td>{item.product.price * item.quantity}</td>
                            <td><button onClick={() => onDeleteProduct(item.product.id)} className="btn btn-danger">Eliminar</button></td>
                        </tr>
                    ))}

                </tbody>
                <tfoot>
                    <tr>
                        <td colSpan={3} className="text-end fw-bold">Total</td>
                        <td colSpan={2} className="text-start fw-bold">${total}</td>
                    </tr>
                </tfoot>
            </table>
            <button
                className="btn btn-success"
                onClick={onCatalog}
            >Seguir comprando</button>
        </>
    )
}

export default CartView

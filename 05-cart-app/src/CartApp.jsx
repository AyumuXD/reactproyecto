import { Navigate, Route, Routes } from "react-router-dom";
import CartView from "./components/CartView";
import CatalogView from "./components/CatalogView";
import { useItemsCart } from "./hooks/useItemsCart";
import NavView from "./components/NavView";
import CartRoutes from "./routes/CartRoutes";

const CartApp = () => {

    const { cartItems, handlerDeleteProductCart, handlerAddProductCart } = useItemsCart();

    return (
        <>
            <NavView/>
            <div className="container my-4">
                <h3>Cart App</h3>

                <CartRoutes 
                    handlerAddProductCart = {handlerAddProductCart}
                    cartItems ={cartItems}
                    handlerDeleteProductCart = {handlerDeleteProductCart}
                />

            </div>
        </>
    )
}

export default CartApp

import PropTypes from 'prop-types';
const ClientView = ({client,title}) => {
    const { name: nameCliente, lastName, address: { country, city, street, number } } = client;
    return (
        <>
            <h3>{title}</h3>
            <ul className="list-group">
                <li className="list-group-item active">{nameCliente}</li>
                <li className="list-group-item">{lastName}</li>
                <li className="list-group-item">{country} / {city}</li>
                <li className="list-group-item">{street} {number}</li>
            </ul>
        </>
    )
}

ClientView.propTypes = {
    client: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired
}

export default ClientView
